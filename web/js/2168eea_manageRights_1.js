$(function() {

    $('#manage_rights_freeAccess').click(function() {
        checkFreeAccess();
    });

    $('#manage_rights_accessibleForManagers, .agency').click(function() {
        checkManagers();
    });

    var checkFreeAccess = function() {
        if ($('#manage_rights_freeAccess').is(':checked')) {
            $('#manage_rights_accessibleForManagers').attr('disabled', true);
            $('#manage_rights_accessibleForManagers').attr('checked', false);
            $('.agency').attr('disabled', true);
            $('.agency').attr('checked', false);
        }
        else {
            $('#manage_rights_accessibleForManagers').attr('disabled', false);
            $('.agency').attr('disabled', false);
        }
    };

    var checkManagers = function() {
        if ($('#manage_rights_accessibleForManagers, .agency').is(':checked')) {
            $('#manage_rights_freeAccess').attr('disabled', true);
            $('#manage_rights_freeAccess').attr('checked', false);
        }
        else if (! $('#manage_rights_accessibleForManagers:checked, .agency:checked').length)
            $('#manage_rights_freeAccess').attr('disabled', false);
    };

    checkFreeAccess();
    checkManagers();

});
