$(function() {

    var errors = $('#errors');
    var ok;

    $('#submit').click(function() {
        errors.empty();
        ok = true;

        if ($('#password').val().length < 7)
            addError('Le mot de passe doit faire au moins 7 caractères.');
        if ($('#password').val() !== $('#password_conf').val())
            addError('Le mot de passe de confirmation doit être identique au mot de passe.');

        if (ok)
            errors.addClass('hidden');
        else {
            errors.removeClass('hidden');
            return false;
        }
    });

    var addError = function(error) {
        errors.append($('<p>' + error + '</p>'));
        ok = false;
    };
});
