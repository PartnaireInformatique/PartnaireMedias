/**
 * Upload Ajax de videos
 * @author Raphaël ALVES
 */

$(function() {

    var uploadButton = $('#up_button'); // (DOM) bouton d'upload
    var validationButton = $('#validation'); // (DOM) bouton de validation
    var filesList = $('#filesList'); // (DOM) liste des fichiers
    var loadingtext = $('#loadingtext'); // (DOM) Text de chargement
    var files = {}; // liste des fichiers avec ordre et légende
    var sortable; // lib pour le tri

    $(document).on({
        ajaxStart: function() {
            validationButton.prop('disabled', true);
        },
        ajaxStop: function() {
            validationButton.prop('disabled', false);
        }
    });


    /**
     * Ajout de la classe onDragAndDrop au démarrage du drag&Drop.
     */
    $(document).on('dragenter', '#mainwell', function() {
        $(this).addClass('onUpload');

        return false;
    });

    /**
     * Ajout de la classe onDragAndDrop au démarrage du drag&Drop.
     */
    $(document).on('dragover', '#mainwell', function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).addClass('onDragAndDrop');

        return false;
    });

    /**
     * Si on stope le Drag&drop, on supprime la classe onDragAndDrop.
     */
    $(document).on('dragleave', '#mainwell', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).removeClass('onDragAndDrop');

        return false;
    });

    /**
     * Gestion des fichiers à la fin du drag&Drop.
     */
    $(document).on('drop', '#mainwell', function(e) {
        if (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files.length) {
            var dragAndDropFiles = e.originalEvent.dataTransfer.files;
            if (areAllMp4(dragAndDropFiles)) {
                e.preventDefault();
                e.stopPropagation();
                $(this).removeClass('onDragAndDrop');
                addFiles(dragAndDropFiles);
            }
            else {
                $(this).removeClass('onDragAndDrop');
                $(this).addClass('onDragAndDropError');
                alert('Vous ne pouvez ajouter que des fichiers mp4.');
                $(this).removeClass('onDragAndDropError');
            }
        }

        return false;
    });

    /**
     * Appelle la fonction addFiles au click sur le bouton d'upload.
     */
    uploadButton.change(function() {
        addFiles($(this)[0].files);
    });

    /**
     * Effectue les opérations nécessaires à la validation.
     */
    validationButton.click(function() {
        animateButton();
        update();
    });

    /**
     * Ajoute les nouveaux fichiers sélectionnés dans le dictionnaire files puis les upload.
     */
    var addFiles = function(newFiles) {
        $('#empty').remove();
        var nb = count(files);
        for (var i = 0, l = newFiles.length ; i < l ; i ++) {
            file = newFiles[i];
            files[nb + i] = {
                file: file,
                name: file.name,
                legend: '',
                order: nb + i,
            };
            displayFile(files[nb + i]);
            firstUpload(files[nb + i]);
        }
        prepareDeletion();
        initializeSortable();
    };

    /**
     * Permet d'afficher un fichier.
     */
    var displayFile = function(file) {
        filesList.append($('\
            <li class="list-group-item" data-index="' + file.order +'" data-id="' + (file.id == undefined ? '' : file.id) + '">\
                <div class="row">\
                    <div class="col-sm-12">\
                        <p>' + file.name + '</p>\
                    </div>\
                </div>\
                <div class="row">\
                    <div class="col-sm-5">\
                        <input type="text" class="form-control" placeholder="Légende (facultatif)" value="' + ((file.legend != null && file.legend != undefined) ? file.legend : '') + '" />\
                    </div>\
                    <button class="btn btn-danger btn-embossed delete-button">\
                        <span class="fui-trash"></span>\
                    </button>\
                </div>\
                <br />\
                <div class="row">\
                    <div class="col-sm-12">\
                        <div class="progress hidden">\
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">\
                                <span class="sr-only">0% Complete</span>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            </li>'));
    };

    /**
     * Premier upload lorsqu'un fichier est ajouté, avant toute modification par l'utilisateur.
     */
    var firstUpload = function(file) {
        var data = new FormData;
        data.append('order', file.order);
        data.append('file', file.file);

        var elem = filesList.find('li[data-index="' + file.order + '"]').eq(0);
        elem.find('div[class="progress hidden"]').eq(0).removeClass('hidden');
        elem.find('button').eq(0).prop('disabled', true);

        $.ajax({
            type: 'POST',
            url: upload_route,
            data: data,
            contentType: false,
            processData: false,
            success: function(response) {
                file.id = response.id;
                filesList.find('li[data-index="' + file.order + '"]').eq(0).data('id', response.id);
                elem.find('button').eq(0).prop('disabled', false);
            },
            xhr: function() {
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.addEventListener('progress', function(e) {
                    filesList.find('li[data-index=' + file.order + ']')
                             .eq(0)
                             .find('div[class="progress"]')
                             .find('div')
                             .css('width', e.loaded / e.total * 100 + '%');
                }, false);

                return xhr;
            }
        });
    };

    /**
     * Mise à jour des photos avec légende et ordre lors de la validation.
     */
    var secondUpload = function(file) {
        $.ajax({
            type: 'POST',
            url: Routing.generate('back_update_file_ajax', {id : file.id}),
            data: {order : file.order, legend : file.legend},
            dataType: 'json',
            success: function(response) {
                console.log(response);
            }
        });
    };

    /**
     * Mise à jour de tous les éléments, lorsque l'on clique sur "valider".
     */
    var update = function() {
        $('.load-modal').toggle();
        loadingtext.text('Upload en cours veuillez patienter...');

        filesList.find('li').each(function(index, element) {
            textField = $(element).find('input[type="text"]');
            files[$(element).data('index')].legend = textField.val();
            textField.prop('disabled', true);
        });

        for (var key in files)
            secondUpload(files[key]);

        $(document).on({
            ajaxStop: function() {
                window.location.href= $('#rights_route').data('href');
            }
        });
    };

    /**
     * Télécharge toutes les photos de l'article si l'on est sur le cas d'une mise à jour.
     */
    var downloadContent = function() {
        $('.load-modal').toggle();
        $.ajax({
            type: 'GET',
            url: Routing.generate('back_download_article_ajax', {id : $('#update').data('id')}),
            success: function(response) {
                console.log(response);
                var newFiles = response.files;
                $('#empty').remove();
                var nb = count(files);
                for (var i = 0, l = newFiles.length ; i < l ; i ++) {
                    file = newFiles[i];
                    files[nb + i] = {
                        id: file.id,
                        file: file,
                        name: file.name,
                        legend: file.legend
                    };
                    displayFile(file);
                }
                initializeSortable();
                prepareDeletion();
                validationButton.prop('disabled', false);
                $('.load-modal').toggle();
            }
        });
    };

    /**
     * Prépare tous les éléments pour réagir au click sur la corbeille.
     */
    var prepareDeletion = function() {
        $('.delete-button').unbind('click');
        $('.delete-button').click(function() {
            var elem = $(this).closest('li');
            var id = elem.data('id');
            var key = elem.data('index');
            $.ajax({
                type: 'DELETE',
                url: Routing.generate('back_delete_file_ajax', {id : id}),
                success: function(response) {
                    delete files[key];
                    elem.hide('slow', function() {
                        elem.remove();
                        reindex();
                    });
                }
            });
        });
    };

    /**
     * Retourne le nombre d'entrées dans un dictionnaire, donc
     * ici le nombre de fichiers dans le dictionnaire files.
     */
    var count = function(object) {
        return Object.keys(object).length;
    };

    /**
     * Initialise la lib "Sortable" pour trier les vidéos.
     */
    var initializeSortable = function() {
        sortable = Sortable.create(document.getElementById('filesList'), {
            onSort: reindex
        });
    };

    /**
     * Anime le bouton de validation avec "upload en cours".
     */
    var animateButton = function() {
        validationButton.html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Upload en cours...');
        validationButton.prop('disabled', true);
    };

    /**
     * Réindexe tous les éléments, lorsque l'on modifie l'ordre des vidéos.
     */
    var reindex = function() {
        var newList = {};
        var obj;
        filesList.find('li').each(function(index, element) {
            obj = files[$(element).data('index')];
            obj.order = index;
            newList[index] = obj;
            $(element).data('index', index);
        });

        files = newList;
        console.log('[REINDEX] ', files);
    };

    /**
     * Check si tous les fichiers sont bien des fichiers mp4.
     */
    var areAllMp4 = function(files) {
        for (var i = 0, l = files.length ; i < l ; i ++)
            if (files[i].type.split('/')[1] !== 'mp4')
                return false;

        return true;
    };

    /**
     * Si l'on est dans le cas d'une mise à jour, on télécharge toutes les photos de l'article
     */
    if ($('#update').length) {
        downloadContent();
    }

});
