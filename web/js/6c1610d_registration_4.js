$(function() {

    var errors = $('#errors');
    var ok;

    $('#validation').click(function() {
        errors.empty();
        ok = true;
        var empty = false;

        $('input').each(function(index, element) { if ('' === $(element).val()) empty = true; });
        if (empty)
            addError('Vous devez remplir tous les champs.');
        if ($('#app_user_registration_email').val().indexOf('@partnaire.fr') === -1)
            addError('L\'adresse mail doit être votre adresse mail partnaire.');
        if ($('#app_user_registration_password_first').val() !== $('#app_user_registration_password_second').val())
            addError('Les deux mots de passe doivent être identiques.');
        if ($('#app_user_registration_password_first').val().length < 7)
            addError('Votre mot de passe doit faire au moins 7 caractères.');

        if (ok)
            errors.addClass('hidden');
        else {
            errors.removeClass('hidden');
            return false;
        }
    });

    var addError = function(error) {
        errors.append($('<p>' + error + '</p>'));
        ok = false;
    };
});
