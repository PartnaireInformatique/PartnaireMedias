<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use UserBundle\Entity\User;
use UserBundle\Form\RegistrationType;

class UserController extends Controller
{
    public function indexAction()
    {
        return $this->render('UserBundle:User:index.html.twig', array(
            // ...
        ));
    }

    public function registrationAction(Request $request)
    {
        $user = new User;
        $form = $this->createForm(new RegistrationType, $user);

        if ('POST' === $request->getMethod())
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $user->setPassword(
                    $this->container->get('security.password_encoder')->encodePassword(
                        $user, $user->getPassword()
                    )
                );
                $user->setUsername(explode('@', $user->getEmail())[0]);
                $user->setFirstname(ucfirst($user->getFirstname()));
                $user->setLastname(strtoupper($user->getLastname()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $message = \Swift_Message::newInstance()
                           ->setSubject('Partnaire Médias confirmation')
                           ->setFrom('medias@partnaire.fr')
                           ->setTo($user->getEmail())
                           ->setBody(
                                $this->renderView(
                                    'UserBundle:User:registration_mail.html.twig',
                                    array('user' => $user)
                                ),
                                'text/html'
                            );
                $this->get('mailer')->send($message);

                return $this->redirectToRoute('user_registration_confirmation');
            }
        }

        return $this->render('UserBundle:User:registration.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function registrationConfirmationAction()
    {
        return $this->render('UserBundle:User:registration.html.twig', array('confirmation' => true));
    }

    public function enableAccountAction($salt)
    {
        $response = new Response;
        $response->setStatusCode(200);
        $response->headers->set('Refresh', '7; url='.$this->generateUrl('fos_user_security_login'));

        $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBySalt($salt);

        if (null === $user)
            $response->setContent('<p>Aucun utilisateur, vous allez être redirigé(e).</p>');
        else if (true === $user->isEnabled())
            return $response->setContent('<p>Utilisateur déjà activé, vous allez être redirigé(e).</p>');
        else
        {
            $user->setEnabled(true);
            $this->getDoctrine()->getManager()->flush();
            return $response->setContent(
                '<p>Votre compte est activé, vous pouvez désormais vous connecter.<br />Vous allez être redirigé(e).</p>'
            );
        }

        return $response;
    }

    public function forgottenPasswordAction(Request $request)
    {
        if ('POST' === $request->getMethod())
        {
            $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneByEmail($request->get('email'));
            if (null === $user)
                return $this->render('UserBundle:User:forgotten_password.html.twig', array('error' => true));
            else
            {
                $message = \Swift_Message::newInstance()
                           ->setSubject('Partnaire Médias mot de passe')
                           ->setFrom('medias@partnaire.fr')
                           ->setTo($user->getEmail())
                           ->setBody(
                                $this->renderView(
                                    'UserBundle:User:change_password_mail.html.twig',
                                    array('user' => $user)
                                ),
                                'text/html'
                            );
                $this->get('mailer')->send($message);

                return $this->render('UserBundle:User:forgotten_password.html.twig', array('ok' => true));
            }
        }
        else
            return $this->render('UserBundle:User:forgotten_password.html.twig');
    }

    public function reinitializePasswordAction(Request $request, User $user)
    {
        if ('POST' === $request->getMethod())
        {
            $password = $request->get('password');
            $conf = $request->get('password_conf');

            if ($password !== $conf || strlen($password) < 7)
                return $this->render('UserBundle:User:reinitialize_password.html.twig', array('error' => true));
            else
            {
                $user->setPassword($this->container->get('security.password_encoder')->encodePassword($user, $password));
                $this->getDoctrine()->getManager()->flush();

                return $this->render('UserBundle:User:reinitialize_password.html.twig', array('ok' => true));
            }
        }
        else
            return $this->render('UserBundle:User:reinitialize_password.html.twig');
    }

}
