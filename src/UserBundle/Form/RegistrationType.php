<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', array(
                'required' => true,
                'label' => 'Adresse mail : '
            ))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'Les champs de mots de passe doivent être identiques.',
                'required' => true,
                'first_options' => array('label' => 'Mot de passe : '),
                'second_options' => array('label' => 'Confirmation mot de passe : ')
            ))
            ->add('firstname', 'text', array(
                'required' => true,
                'label' => 'Prénom : '
            ))
            ->add('lastname', 'text', array(
                'required' => true,
                'label' => 'Nom : '
            ))
            ->add('agencyname', 'text', array(
                'required' => true,
                'label' => 'Nom de votre agence : '
            ))
        ;
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User'
        ));
    }
}
