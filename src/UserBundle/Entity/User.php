<?php

namespace UserBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity(
 *  fields={"email"},
 *  message="Adresse mail déjà utilisée."
 * )
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="firstname", type="string", nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(name="lastname", type="string", nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(name="agencyname", type="string", nullable=false)
     * @Assert\NotBlank()
     */
    private $agencyname;

    public function __construct()
    {
        parent::__construct();
        if (empty($this->roles))
            $this->roles[] = 'ROLE_USER';
    }

    public function setRole($role)
    {
        $this->roles[] = $role;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set agencyname
     *
     * @param string $agencyname
     *
     * @return User
     */
    public function setAgencyname($agencyname)
    {
        $this->agencyname = $agencyname;

        return $this;
    }

    /**
     * Get agencyname
     *
     * @return string
     */
    public function getAgencyname()
    {
        return $this->agencyname;
    }

    public function __toString()
    {
        return (null === $this->firstname || null === $this->lastname)
                ? $this->agencyname
                : $this->firstname.' '.$this->lastname;
    }
}
