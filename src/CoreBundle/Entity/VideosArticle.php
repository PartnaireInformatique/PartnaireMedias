<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VideosArticle
 *
 * @ORM\Table(name="videos_article")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\VideosArticleRepository")
 */
class VideosArticle extends BaseArticle
{

}
