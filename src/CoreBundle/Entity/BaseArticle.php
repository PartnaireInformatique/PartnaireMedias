<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * BaseArticle
 *
 * @ORM\Table(name="base_article")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\BaseArticleRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"photos"="PhotosArticle", "videos"="VideosArticle"})
 */
abstract class BaseArticle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     * @Gedmo\Slug(fields={"title"})
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationdate", type="date")
     */
    private $creationdate;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var CoreBundle\Entity\File
     *
     * @ORM\OneToMany(targetEntity="CoreBundle\Entity\File", mappedBy="article", cascade={"remove"})
     * @ORM\OrderBy({"orderNumber" = "asc"})
     */
    private $files;

    /**
     * @var UserBundle\Entity\User
     *
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User")
     */
    private $users;

    /**
     * @var boolean
     *
     * @ORM\Column(name="accessibleForManagers", type="boolean", nullable=false)
     */
    private $accessibleForManagers = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="freeAccess", type="boolean", nullable=false)
     */
    private $freeAccess = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function isVideosArticle()
    {
        return (new \ReflectionClass($this))->getShortName() === 'VideosArticle';
    }

    public function isPhotosArticle()
    {
        return (new \ReflectionClass($this))->getShortName() === 'PhotosArticle';
    }

    public function to_json()
    {
        $array = array(
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'creationdate' => $this->creationdate,
            'accessibleForManagers' => $this->accessibleForManagers,
            'freeAccess' => $this->freeAccess
        );

        $files = array();
        foreach ($this->files as $file)
            $files[] = $file->to_json();

        $array['files'] = $files;

        return $array;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BaseArticle
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return BaseArticle
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set creationdate
     *
     * @param \DateTime $creationdate
     *
     * @return BaseArticle
     */
    public function setCreationdate($creationdate)
    {
        $this->creationdate = $creationdate;

        return $this;
    }

    /**
     * Get creationdate
     *
     * @return \DateTime
     */
    public function getCreationdate()
    {
        return $this->creationdate;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return BaseArticle
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Add file
     *
     * @param \CoreBundle\Entity\File $file
     *
     * @return BaseArticle
     */
    public function addFile(\CoreBundle\Entity\File $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \CoreBundle\Entity\File $file
     */
    public function removeFile(\CoreBundle\Entity\File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set accessibleForManagers
     *
     * @param boolean $accessibleForManagers
     *
     * @return BaseArticle
     */
    public function setAccessibleForManagers($accessibleForManagers)
    {
        $this->accessibleForManagers = $accessibleForManagers;

        return $this;
    }

    /**
     * Get accessibleForManagers
     *
     * @return boolean
     */
    public function getAccessibleForManagers()
    {
        return $this->accessibleForManagers;
    }

    /**
     * Add user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return BaseArticle
     */
    public function addUser(\UserBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \UserBundle\Entity\User $user
     */
    public function removeUser(\UserBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set freeAccess
     *
     * @param boolean $freeAccess
     *
     * @return BaseArticle
     */
    public function setFreeAccess($freeAccess)
    {
        $this->freeAccess = $freeAccess;

        return $this;
    }

    /**
     * Get freeAccess
     *
     * @return boolean
     */
    public function getFreeAccess()
    {
        return $this->freeAccess;
    }
}
