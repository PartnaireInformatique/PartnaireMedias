<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PhotosArticle
 *
 * @ORM\Table(name="photos_article")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\PhotosArticleRepository")
 */
class PhotosArticle extends BaseArticle
{

}
