<?php

namespace FrontEndBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use CoreBundle\Entity\BaseArticle;

class FrontController extends Controller
{
    public function indexAction(Request $request, $page=0)
    {
        return $this->render('FrontEndBundle:Front:home.html.twig', array(
            'photos' => $this->getDoctrine()->getRepository('CoreBundle:PhotosArticle')->findBy(
                array(), array('id' => 'DESC'), 3
            ),
            'videos' => $this->getDoctrine()->getRepository('CoreBundle:VideosArticle')->findBy(
                array(), array('id' => 'DESC'), 3
            )
        ));
    }

    public function videosAction(Request $request, $page=0)
    {
        $maxArticles = $this->container->getParameter('articles_per_page');
        $count = $this->getDoctrine()->getRepository('CoreBundle:VideosArticle')->count();
        $pagination = array(
            'page' => $page,
            'route' => $request->get('_route'),
            'count' => ceil($count / $maxArticles),
            'route_params' => array()
        );

        if ($page > $pagination['count'])
            throw $this->createNotFoundException('La page n\'existe pas.');


        return $this->render('FrontEndBundle:Front:index.html.twig', array(
            'articles' => $this->getDoctrine()->getRepository('CoreBundle:VideosArticle')->getList(
                $page, $maxArticles, $this->getUser()
            ),
            'pagination' => $pagination,
            'title' => 'Articles vidéos publiés'
        ));
    }

    public function photosAction(Request $request, $page=0)
    {
        $maxArticles = $this->container->getParameter('articles_per_page');
        $count = $this->getDoctrine()->getRepository('CoreBundle:PhotosArticle')->count();
        $pagination = array(
            'page' => $page,
            'route' => $request->get('_route'),
            'count' => ceil($count / $maxArticles),
            'route_params' => array()
        );

        if ($page > $pagination['count'])
            throw $this->createNotFoundException('La page n\'existe pas.');


        return $this->render('FrontEndBundle:Front:index.html.twig', array(
            'articles' => $this->getDoctrine()->getRepository('CoreBundle:PhotosArticle')->getList(
                $page, $maxArticles, $this->getUser()
            ),
            'pagination' => $pagination,
            'title' => 'Galleries de photos publiées'
        ));
    }

    public function articleAction(BaseArticle $article)
    {
        if (! $article->getFreeAccess() && ! $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            if (null === $this->getUser())
                return $this->redirectToRoute('fos_user_security_login');
            else if (! $article->getAccessibleForManagers() &&  $this->get('security.authorization_checker')->isGranted('ROLE_MANAGER'))
            {
                $this->addFlashbag('error', 'Cet article n\'est pas accessible pour les managers.');
                return $this->redirect($this->getRequest()->headers->get('referer'));
            }
            else if (! $this->get('security.authorization_checker')->isGranted('ROLE_MANAGER') && ! $article->getUsers()->contains($this->getUser()))
            {
                $this->addFlashbag('error', 'Cet article n\'est pas accessible pour votre agence.');
                return $this->redirect($this->getRequest()->headers->get('referer'));
            }
        }

        return $this->render('FrontEndBundle:Front:article.html.twig', array(
            'article' => $article
        ));
    }

    public function addFlashbag($key, $message)
    {
        $this->getRequest()->getSession()->getFlashbag()->add($key, $message);
    }


}
