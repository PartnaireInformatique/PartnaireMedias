<?php

namespace BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

use Doctrine\ORM\EntityRepository;

class ManageRightsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('users', 'entity', array(
                'class' => 'UserBundle:User',
                'label' => 'Agences avec accès : ',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                              ->where('u.roles LIKE :role')
                              ->setParameter('role', '%ROLE_AGENCY%');
                    },
                'multiple' => true,
                'expanded' => true,
                'required' => false
            ))
            ->add('accessibleForManagers', 'checkbox', array(
                'label' => 'Accessible pour les managers : ',
                'required' => false
            ))
            ->add('freeAccess', 'checkbox', array(
                'label' => 'Accessible pour tout le monde : ',
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\BaseArticle'
        ));
    }
}
