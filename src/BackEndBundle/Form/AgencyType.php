<?php

namespace BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgencyType extends AbstractType
{
    /**
     * @param array $options
     * @param FormBuilderInterface $builder
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('agencyname', 'text', array(
                'label' => 'Nom de l\'agence : ',
                'required' => true
            ))
            ->add('email', 'email', array(
                'label' => 'Adresse mail de l\'agence : ',
                'required' => true
            ))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'Les champs de mots de passe doivent être identiques.',
                'required' => true,
                'first_options' => array('label' => 'Mot de passe : '),
                'second_options' => array('label' => 'Confirmation mot de passe : ')
            ))
        ;
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User'
        ));
    }
}
