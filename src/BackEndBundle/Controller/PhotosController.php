<?php

namespace BackEndBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use CoreBundle\Entity\PhotosArticle;
use CoreBundle\Entity\File;

class PhotosController extends BackEndController
{

    public function uploadPhotosFormAction(PhotosArticle $article)
    {
        if (! $article->getFiles()->isEmpty())
            return $this->redirectToRoute('front_index');
        else
            return $this->render('BackEndBundle:Back:uploadPhotos.html.twig', array(
                'id' => $article->getId(),
                'slug' => $article->getSlug()
            ));
    }

    public function updatePhotosFormAction(PhotosArticle $article)
    {
        return $this->render('BackEndBundle:Back:uploadPhotos.html.twig', array(
            'id' => $article->getId(),
            'slug' => $article->getSlug(),
            'update' => true
        ));
    }

    /**
     * Uploader un fichier (méthode Ajax)
     * @param Symfony\Component\HttpFoundation\Request La requête
     * @param integer $id Id de l'iqf auquel sera rattaché l'objet
     * @return Symfony\Component\HttpFoundation\JsonResponse Les données sous forme Json
     */
    public function uploadPhotoAction(Request $request, PhotosArticle $article)
    {
        $em = $this->getManager();
        $photo = new File;
        $photo->setFile($request->files->get('file'));
        $photo->setName($request->files->get('file')->getClientOriginalName());
        $photo->setLegend($request->get('legend'));
        $photo->setOrderNumber($request->get('order'));
        $photo->setArticle($article);

        $em->persist($photo);

        // Upload automatique grâce a l'extension doctrine.
        // La supression du fichier se fera automatiquement si l'entité est supprimée.
        $this->get('stof_doctrine_extensions.uploadable.manager')
             ->markEntityToUpload($photo, $photo->getFile());

        $em->flush();

        return new JsonResponse($photo->to_json());
    }

    public function getThumbnailAction(Request $request, File $photo)
    {
        $path = explode('/', $photo->getPath());
        $path = 'uploads/'.$path[count($path) - 1];

        // RedirectResponse object
        return $this->container
            ->get('liip_imagine.controller')
            ->filterAction(
                $request,   // http request
                $path,      // original image you want to apply a filter to
                'thumbnail' // filter defined in config.yml
            );
    }
}
