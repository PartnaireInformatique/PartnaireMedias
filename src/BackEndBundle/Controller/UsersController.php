<?php

namespace BackEndBundle\Controller;

use UserBundle\Entity\User;

class UsersController extends BackEndController
{
    public function deleteUserAction(User $user)
    {
        $em = $this->getManager();

        if (! $user->hasRole('ROLE_SUPERUSER'))
        {
            $em->remove($user);
            $em->flush();
        }
        $this->addFlashbag('success', 'Suppression effectuée.');

        return $this->redirectToRoute('back_index');
    }

    public function managerAction(User $user)
    {
        if ($user->hasRole('ROLE_MANAGER'))
        {
            $this->addFlashbag('success', $user->getFirstname().' '.$user->getLastname().' n\'est désormais plus manager.');
            $user->removeRole('ROLE_MANAGER');
        }
        else
        {
            $this->addFlashbag('success', $user->getFirstname().' '.$user->getLastname().' est désormais manager.');
            $user->addRole('ROLE_MANAGER');
        }

        $this->getManager()->flush();

        return $this->redirectToRoute('back_index');
    }

    public function adminAction(User $user)
    {
        if ($user->hasRole('ROLE_ADMIN'))
        {
            $user->removeRole('ROLE_ADMIN');
            $this->addFlashbag('success', $user->getFirstname().' '.$user->getLastname().' n\'est désormais plus administrateur.');
        }

        else
        {
            $user->addRole('ROLE_ADMIN');
            $this->addFlashbag('success', $user->getFirstname().' '.$user->getLastname().' est désormais administrateur.');
        }

        $this->getManager()->flush();

        return $this->redirectToRoute('back_index');
    }
}
