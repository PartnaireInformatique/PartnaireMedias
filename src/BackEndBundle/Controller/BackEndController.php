<?php

namespace BackEndBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\FormError;

use BackEndBundle\Form\AgencyType;

use UserBundle\Entity\User;

class BackEndController extends Controller
{
    public function indexAction(Request $request)
    {
        $agency = new User;
        $form = $this->createForm(new AgencyType, $agency);

        if ('POST' === $request->getMethod())
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                if (null === $this->getManager()->getRepository(
                    'UserBundle:User'
                )->findOneByUsername($agency->getAgencyname()))
                {
                    $agency->setPassword(
                        $this->container->get('security.password_encoder')->encodePassword(
                            $agency, $agency->getPassword()
                        )
                    );
                    $agency->setUsername($agency->getAgencyname());
                    $agency->addRole('ROLE_AGENCY');
                    $agency->setEnabled(true);
                    $em = $this->getManager();
                    $em->persist($agency);
                    $em->flush();
                    $this->addFlashbag('success', 'Agence créée.');
                }
                else
                {
                    $form->get('agencyname')->addError(new FormError('Agence déjà créée'));
                    $this->addFlashbag('error', 'Erreur de saisie dans le formulaire.');
                }
            }
            else
                $this->addFlashbag('error', 'Erreur de saisie dans le formulaire.');
        }

        return $this->render('BackEndBundle:Back:index.html.twig', array(
            'articles' => $this->getManager()->getRepository('CoreBundle:BaseArticle')->findby(
                array(), array('id' => 'DESC'), $this->container->getParameter('admin_preview_number')
            ),
            'agencies' => $this->getDoctrine()->getRepository('UserBundle:User')->getAgenciesWithoutCurrentUser(
                $this->getUser(), $this->container->getParameter('admin_preview_number')
            ),
            'users' => $this->getDoctrine()->getRepository('UserBundle:User')->getUsersWithoutCurrent(
                $this->getUser(), $this->container->getParameter('admin_preview_number')
            ),
            'form' => $form->createView()
        ));
    }

    public function manageArticlesAction()
    {
        return $this->render('BackEndBundle:Back:manageArticles.html.twig', array(
            'articles' => $this->getManager()->getRepository('CoreBundle:BaseArticle')->findby(
                array(), array('id' => 'DESC')
            )
        ));
    }

    public function manageAgenciesAction()
    {
        return $this->render('BackEndBundle:Back:manageAgencies.html.twig', array(
            'agencies' => $this->getDoctrine()->getRepository('UserBundle:User')->getAgenciesWithoutCurrentUser(
                $this->getUser()
            ),
        ));
    }

    public function manageUsersAction()
    {
        return $this->render('BackEndBundle:Back:manageUsers.html.twig', array(
            'users' => $this->getDoctrine()->getRepository('UserBundle:User')->getUsersWithoutCurrent(
                $this->getUser()
            ),
        ));
    }

    public function changelogAction()
    {
        return $this->render('BackEndBundle:Back:changelog.html.twig');
    }

    /*=========================================*/
    /*---------- METHODES GENERIQUES ----------*/
    /*=========================================*/

    public function addFlashbag($key, $message)
    {
        $this->getRequest()->getSession()->getFlashbag()->add($key, $message);
    }

    public function getManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
