<?php

namespace BackEndBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use CoreBundle\Entity\VideosArticle;
use CoreBundle\Entity\File;

use BackEndBundle\Form\BaseArticleType;

class VideosController extends BackEndController
{
    public function uploadVideosFormAction(VideosArticle $article)
    {
        if (! $article->getFiles()->isEmpty())
            return $this->redirectToRoute('front_index');
        else
            return $this->render('BackEndBundle:Back:uploadVideos.html.twig', array(
                'id' => $article->getId(),
                'slug' => $article->getSlug(),
            ));
    }

    public function updateVideosFormAction(VideosArticle $article)
    {
        return $this->render('BackEndBundle:Back:uploadVideos.html.twig', array(
            'id' => $article->getId(),
            'slug' => $article->getSlug(),
            'update' => true
        ));
    }

    /**
     * Uploader un fichier (méthode Ajax)
     * @param Symfony\Component\HttpFoundation\Request La requête
     * @param integer $id Id de l'iqf auquel sera rattaché l'objet
     * @return Symfony\Component\HttpFoundation\JsonResponse Les données sous forme Json
     */
    public function uploadVideoAction(Request $request, VideosArticle $article)
    {
        $em = $this->getManager();
        $video = new File;
        $video->setFile($request->files->get('file'));
        $video->setName($request->files->get('file')->getClientOriginalName());
        $video->setLegend($request->get('legend'));
        $video->setOrderNumber($request->get('order'));
        $video->setArticle($article);

        $em->persist($video);

        // Upload automatique grâce a l'extension doctrine.
        // La supression du fichier se fera automatiquement si l'entité est supprimée.
        $this->get('stof_doctrine_extensions.uploadable.manager')
             ->markEntityToUpload($video, $video->getFile());

        $em->flush();

        // On retourne l'id du fichier dans un tableau JSON
        return new JsonResponse($video->to_json());
    }
}
