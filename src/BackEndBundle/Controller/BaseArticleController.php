<?php

namespace BackEndBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use CoreBundle\Entity\BaseArticle;
use CoreBundle\Entity\PhotosArticle;
use CoreBundle\Entity\VideosArticle;
use CoreBundle\Entity\File;

use BackEndBundle\Form\BaseArticleType;
use BackEndBundle\Form\ManageRightsType;

class BaseArticleController extends BackEndController
{
    public function newArticleAction(Request $request, $type)
    {
        if ('videos' === $type)
            $article = new VideosArticle;
        else if ('photos' === $type)
            $article = new PhotosArticle;

        $form = $this->createForm(new BaseArticleType, $article);

        if ('POST' === $request->getMethod())
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $article->setCreationdate(new \DateTime);
                $em = $this->getManager();
                $em->persist($article);
                $em->flush();

                if ('videos' === $type)
                    return $this->redirectToRoute('back_upload_videos_form', array(
                        'id' => $article->getId()
                    ));
                else
                    return $this->redirectToRoute('back_upload_photos_form', array(
                        'id' => $article->getId()
                    ));
            }
        }

        return $this->render('BackEndBundle:Back:newArticle.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function updateArticleAction(Request $request, BaseArticle $article)
    {
        $form = $this->createForm(new BaseArticleType, $article);

        if ('POST' === $request->getMethod())
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $this->getManager()->flush();

                if ($article->isVideosArticle())
                    return $this->redirectToRoute('back_update_videos_form', array(
                        'id' => $article->getId()
                    ));
                else if ($article->isPhotosArticle())
                    return $this->redirectToRoute('back_update_photos_form', array(
                        'id' => $article->getId()
                    ));
            }
        }

        return $this->render('BackEndBundle:Back:newArticle.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function updateFileAction(Request $request, File $file)
    {
        $file->setOrderNumber($request->get('order'));
        $file->setLegend($request->get('legend'));
        $this->getManager()->flush();

        return new JsonResponse($file->to_json());
    }

    public function deleteFileAction(File $file)
    {
        $em = $this->getManager();
        $em->remove($file);
        $em->flush();

        return new JsonResponse('success');
    }

    public function deleteAction(BaseArticle $article)
    {
        $em = $this->getManager();
        $em->remove($article);
        $em->flush();
        $this->addFlashbag('success', 'Article bien supprimé.');

        return $this->redirectToRoute('back_index');
    }

    public function downloadAction(BaseArticle $article)
    {
        return new JsonResponse($article->to_json());
    }

    public function manageRightsAction(Request $request, BaseArticle $article)
    {
        $form = $this->createForm(new ManageRightsType, $article);

        if ('POST' === $request->getMethod())
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $em = $this->getManager();
                $em->flush();

                return $this->redirectToRoute('front_article', array(
                    'slug' => $article->getSlug()
                ));
            }
        }

        return $this->render('BackEndBundle:Back:manageRights.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
