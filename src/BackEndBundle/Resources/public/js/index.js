$(function() {

    var form = $('#addagency');
    var showform = $('#showform');
    var ok = true;
    var errors = $('#errors_js');

    if (! $('#error_flashbag').length) {
        var visible = false;
        form.hide();
    }
    else {
        $(document).scrollTop(form.offset().top);
        showform.html('<span class="glyphicon glyphicon-minus"></span> Cacher le formulaire');
        var visible = true;
    }

    showform.click(function() {
        if (visible) {
            showform.html('<span class="glyphicon glyphicon-plus"></span> Ajouter une agence');
            form.slideUp(500);
        }
        else {
            showform.html('<span class="glyphicon glyphicon-minus"></span> Cacher le formulaire');
            form.slideDown(500);
        }
        visible = !visible;
    });

    form.submit(function() {
        if ($('#app_user_registration_password_first').val() !== $('#app_user_registration_password_second').val())
            addError('Les deux mots de passe doivent être identiques.');
        if ($('#app_user_registration_email').val().indexOf('@partnaire.fr') === -1)
            addError('L\'adresse mail doit être une adresse partnaire');

        if (! ok) {
            ok = true;
            return false;
        }
        else
            errors.addClass('hidden');
    });

    var addError = function(error) {
        if (ok)
            errors.find('#errors_js_content').empty();
        errors.removeClass('hidden');
        errors.find('#errors_js_content').append($('<p>' + error + '</p>'));
        ok = false;
    };

});
