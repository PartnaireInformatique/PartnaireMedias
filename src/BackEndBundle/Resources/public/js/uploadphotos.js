/**
 * Upload Ajax de photos
 * @author Raphaël ALVES
 */

$(function() {

    var uploadButton = $('#up_button'); // (DOM) bouton d'upload
    var validationButton = $('#validation'); // (DOM) bouton de validation
    var filesList = $('#filesList'); // (DOM) liste des fichiers
    var loadingtext = $('#loadingtext'); // (DOM) Text de chargement
    var files = {}; // liste des fichiers avec ordre et légende
    var sortable; // lib pour le tri

    /**
     * Actions a effectuer au début et à la fin des requêtes Ajax.
     * Affiche le message de chargement au début.
     * Cache le message de chargement à la fin, réinitialise Sortable, et appelle prepareDeletion.
     */
    $(document).on({
        ajaxStart: function() {
            $('.load-modal').toggle();
        },
        ajaxStop: function() {
            initializeSortable();
            reindex();
            prepareDeletion();
            $('.load-modal').toggle();
        }
    });

    /**
     * Ajout de la classe onDragAndDrop au démarrage du drag&Drop.
     */
    $(document).on('dragenter', '#mainwell', function() {
        $(this).addClass('onUpload');

        return false;
    });

    /**
     * Ajout de la classe onDragAndDrop au démarrage du drag&Drop.
     */
    $(document).on('dragover', '#mainwell', function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).addClass('onDragAndDrop');

        return false;
    });

    /**
     * Si on stope le Drag&drop, on supprime la classe onDragAndDrop.
     */
    $(document).on('dragleave', '#mainwell', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).removeClass('onDragAndDrop');

        return false;
    });

    /**
     * Gestion des fichiers à la fin du drag&Drop.
     */
    $(document).on('drop', '#mainwell', function(e) {
        if (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files.length) {
            var dragAndDropFiles = e.originalEvent.dataTransfer.files;
            if (areAllImages(dragAndDropFiles)) {
                e.preventDefault();
                e.stopPropagation();
                $(this).removeClass('onDragAndDrop');
                addFiles(dragAndDropFiles);
            }
            else {
                $(this).removeClass('onDragAndDrop');
                $(this).addClass('onDragAndDropError');
                alert('Vous ne pouvez ajouter que des images.');
                $(this).removeClass('onDragAndDropError');
            }
        }

        return false;
    });

    /**
     * Appelle la fonction addFiles au click sur le bouton d'upload.
     */
    uploadButton.change(function() {
        addFiles($(this)[0].files);
    });

    /**
     * Effectue les opérations nécessaires à la validation.
     */
    validationButton.click(function() {
        loadingtext.text('Upload en cours veuillez patienter...');
        animateButton();
        update();
    });

    /**
     * Anime le bouton de validation avec "upload en cours".
     */
    var animateButton = function() {
        validationButton.html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Upload en cours...');
        validationButton.prop('disabled', true);
    };

    /**
     * Ajoute les nouveaux fichiers sélectionnés dans le dictionnaire files puis les upload.
     */
    var addFiles = function(newFiles) {
        loadingtext.text('Upload en cours veuillez patienter...');
        $('#empty').remove();
        var nb = count(files);
        for (var i = 0, l = newFiles.length ; i < l ; i ++) {
            file = newFiles[i];
            files[nb + i] = {
                file: file,
                name: file.name,
                legend: ''
            };
            firstUpload(nb + i, files[nb + i]);
        }
        validationButton.prop('disabled', false);
        console.log('[ADDFILES] ', files);
    };

    /**
     * Premier upload lorsqu'un fichier est ajouté, avant toute modification par l'utilisateur.
     */
    var firstUpload = function(key, file) {
        var data = new FormData;
        data.append('order', key);
        data.append('file', file.file);

        $.ajax({
            type: 'POST',
            url: upload_route,
            data: data,
            contentType: false,
            processData: false,
            cache: false,
            success: function(response) {
                displayFile(response);
                files[key].id = response.id;
                console.log(response);
            }
        });
    };

    /**
     * Mise à jour des photos avec légende et ordre lors de la validation.
     */
    var secondUpload = function(key) {
        $.ajax({
            type: 'POST',
            url: Routing.generate('back_update_file_ajax', {id : files[key].id}),
            data: {order : key, legend : files[key].legend},
            dataType: 'json',
            success: function(response) {
                console.log(response);
            }
        });
    };

    /**
     * Mise à jour de tous les éléments, lorsque l'on clique sur "valider".
     */
    var update = function() {

        filesList.find('li').each(function(index, element) {
            textField = $(element).find('input[type="text"]');
            files[$(element).data('index')].legend = textField.val();
            textField.prop('disabled', true);
        });

        for (var key in files)
            secondUpload(key);

        $(document).on({
            ajaxStop: function() {
                window.location.href= $('#redirect_link').data('href');
            }
        });
    };

    /**
     * Affiche un fichier uploadé sur la page.
     */
    var displayFile = function(file) {
        filesList.append($('\
            <li data-id="' + file.id + '" data-index="' + file.order + '" class="col-sm-3">\
                <div class="row text-center">\
                    <img class="upload-preview" src="' + Routing.generate('back_get_thumbnail', {id : file.id}) + '" />\
                </div>\
                <div class="row text-center">\
                    <div class="col-sm-offset-1 col-sm-10">\
                        <input class="form-control" type="text" placeholder="Légende" value="' + ((file.legend != null && file.legend != undefined) ? file.legend : '') + '">\
                    </div>\
                </div>\
                <div class="row text-center">\
                    <button class="btn btn-danger btn-embossed delete-button">\
                        <span class="fui-trash"></span>\
                    </button>\
                </div>\
            </li>'
        ));
    };

    /**
     * Retourne le nombre d'entrées dans un dictionnaire, donc
     * ici le nombre de fichiers dans le dictionnaire files.
     */
    var count = function(object) {
        return Object.keys(object).length;
    };

    /**
     * Initialise la lib "Sortable" pour trier les photos.
     */
    var initializeSortable = function() {
        sortable = Sortable.create(document.getElementById('filesList'), {
            onSort: reindex
        });
    };

    /**
     * Réindexe tous les éléments, lorsque l'on modifie l'ordre des photos.
     */
    var reindex = function() {
        var newList = {};
        var obj;
        filesList.find('li').each(function(index, element) {
            obj = files[$(element).data('index')];
            newList[index] = obj;
            $(element).data('index', index);
        });

        files = newList;
        console.log('[REINDEX] ', files);
    };

    /**
     * Prépare tous les éléments pour réagir au click sur la corbeille.
     */
    var prepareDeletion = function() {
        $('.delete-button').unbind('click');
        $('.delete-button').click(function() {
            loadingtext.text('Suppression en cours veuillez patienter...');
            var elem = $(this).closest('li');
            var id = elem.data('id');
            var key = elem.data('index');
            $.ajax({
                type: 'DELETE',
                url: Routing.generate('back_delete_file_ajax', {id : id}),
                success: function(response) {
                    delete files[key];
                    elem.hide('slow', function() {
                        elem.remove();
                        reindex();
                    });
                }
            });
        });
    };

    /**
     * Télécharge toutes les photos de l'article si l'on est sur le cas d'une mise à jour.
     */
    var downloadContent = function() {
        $.ajax({
            type: 'GET',
            url: Routing.generate('back_download_article_ajax', {id : $('#update').data('id')}),
            success: function(response) {
                console.log(response);
                var newFiles = response.files;
                $('#empty').remove();
                var nb = count(files);
                for (var i = 0, l = newFiles.length ; i < l ; i ++) {
                    file = newFiles[i];
                    files[nb + i] = {
                        id: file.id,
                        file: file,
                        name: file.name,
                        legend: file.legend
                    };
                    displayFile(file);
                }
                reindex();
                validationButton.prop('disabled', false);
                console.log(files);
            }
        });
    };

    /**
     * Check si tous les fichiers sont bien des images.
     */
    var areAllImages = function(files) {
        for (var i = 0, l = files.length ; i < l ; i ++)
            if (files[i].type.split('/')[0] !== 'image')
                return false;

        return true;
    };

    /**
     * Si l'on est dans le cas d'une mise à jour, on télécharge toutes les photos de l'article
     */
    if ($('#update').length) {
        downloadContent();
        loadingtext.text('Chargement en cours veuillez patienter...');
    }

});
